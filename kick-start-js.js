// var name = "Shakila"
// name = "Alamelu"
// var name = "Shakila"
// console.log("Name", name)

// let fName = "Shakila"
// fName = "Shakila"
// console.log("Fname", fName)

// const lName = "Saami durai"
// // lName = "Seetha"
// console.log("LName", lName)

// function add(a, b) {
//   return a + b
// }

// console.log("ADDTION", add(123, 123))
// console.log("ADDTION", add(13, 123))
// console.log("ADDTION", add(1323, 123))
// console.log("ADDTION", add(123, 1433423))

// Variables var, let, const
var fName = "Sarguna"
fName = "Seetha"
var fName = "Shakila"
console.log("fName", fName)

let lName = "samidurai"
// let lName = "Saamidurai"
console.log("lName", lName)

const dob = "14-04-2018"
// dob = "14-04-2020"
console.log("dob", dob)

function multiply() {
  const a = 10
  const b = 20
  const c = a * b
  return c
}

const divide = (a, b) => {
  const c = a / b
  return c
}

class Sameple {
  constructor(name, dob) {
    console.log("This is constructor")
    this.name = name
    this.dob = dob
  }

  // substract = (a, b) => {
  //   return a-b
  // }

  substract = (a, b) => a - b
}

const sample = new Sameple("Sarguna", "14-04-2018")
console.log("Substract", sample.substract(100, 20))
console.log("DOB", sample.dob)

console.log("Mutiply", multiply())
console.log("Divide", divide(1, 2))
console.log("Divide", divide(21, 255))
console.log("Divide", divide(31, 23))
console.log("Divide", divide(41, 22))
console.log("Divide", divide(51, 21))
console.log("Divide", divide(671, 28))
console.log("Divide", divide(18, 20))
