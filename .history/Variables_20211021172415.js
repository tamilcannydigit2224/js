/* VAR */
// var name = "Nithees";
// var name = "Gokul";
// var name = "Mukilarasi";
// name = "Canny Digital";
// console.log(name);

/* LET */
// let name = "Nithees";
// name = "Canny Digital";
// console.log(name);

/* CONST */
// const name = "Nithees";
// name = "Canny Digital";
// console.log(name);

/* SCOPE */
/* BLOCK */
// let name = "My Name";
// const location = "Velachery";

// test();
// // sample();
// function test() {
//   /* FUNCTION */
//   var age = 27;
//   console.log(name);
//   console.log(location);
//   console.log(age);
//   sample();
// }

// function sample() {
//   var address = "Chennai";
//   console.log(name);
//   console.log(location);
//   console.log(address);
//   // console.log(age);
// }

/* HOISTING */
// x = 8;
// console.log(x);
// var x;

// x = 8;
// console.log(x);
// let x;

x = 8;
console.log(x);
const x;