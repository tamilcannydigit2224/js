// var name = "Nithees";
// var name = "Gokul";
// var name = "Mukilarasi";
// name = "Canny Digital";
// console.log(name);

// let name = "Nithees";
// name = "Canny Digital";
// console.log(name);

// const name = "Nithees";
// name = "Canny Digital";
// console.log(name);

// SCOPE
let name = "My Name";
const location = "Velachery";
var time = 10;

function test() {
  var age = 27;
  console.log(name);
  console.log(location);
}

function sample() {
  var address = "Chennai";
  console.log(name);
  console.log(location);
}
